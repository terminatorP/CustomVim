source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      if empty(&shellxquote)
        let l:shxq_sav = ''
        set shellxquote&
      endif
      let cmd = '"' . $VIMRUNTIME . '\diff"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3
  if exists('l:shxq_sav')
    let &shellxquote=l:shxq_sav
  endif
endfunction
" ______________________ VUNDLE ___________________

" Begin Vundle
filetype off

" Set the runtime path to include Vundle and initialize
set rtp+=$VIM/vimfiles/bundle/Vundle.vim/
call vundle#begin('$VIM/vimfiles/bundle')

" Let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Bundles
Plugin 'bling/vim-airline'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-bundler'
Plugin 'tpope/vim-fugitive'
Plugin 'mattn/emmet-vim'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'
Plugin 'pangloss/vim-javascript'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'mkitt/tabline.vim'
Plugin 'tpope/vim-surround'
Plugin 'townk/vim-autoclose'
" Colorschemes
Plugin 'dracula/vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'jnurmine/zenburn'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Use the colorscheme from above
syntax enable
set background=light
color solarized
" colorscheme Tomorrow-Night-Bright
" colorscheme Molokai
" ---------------------- STUFF -------------------

" Specify .md files as markdown
au BufRead,BufNewFile *.md setlocal ft=markdown
" Settings
syntax on
set number
set ts=2
set sw=2 
set expandtab
set splitbelow
set splitright
set autoindent
set encoding=utf-8
"set guifont=DejaVu\ Sans\ Mono:h11
set guifont=Fira\ Code:h11
" set guifont=DejaVu\ Sans\ Mono\ for\ Powerline:h11:b
" disable bell
set belloff=all
" Remove the menu bar, tool bar and scroll bars in guiVim
set go-=m
set go-=T
set go-=r
set go-=e
set go-=L
set go-=l
set go-=R
set sessionoptions+=tabpages,globals
set cc=120
set wrap
set smartindent
set cindent
" Intelligent searching
set smartcase
set incsearch " Incremental search
set hlsearch " Hilight search results

" NERDTree maps
map <C-\> :NERDTreeToggle<CR>
" NERDTree on current directory
map <C-o> :NERDTreeToggle %<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:NERDTreeWinSize=20
" Add close button in tabs
let g:tablineclosebutton=1
map <Leader>n <plug>NERDTreeTabsToggle<CR>
" Disable Arrow keys in Escape mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
" Set airline theme
let g:airline_theme='zenburn'
let g:airline_powerline_fonts=0
" Set the temporary directory for all swaps and backup files
set backup
set backupdir=C:\WINDOWS\Temp
set backupskip=C:\WINDOWS\Temp\*
set directory=C:\WINDOWS\Temp
set undodir=C:\WINOWS\Temp
set writebackup
" set default window size
if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window.
  set lines=999 columns=999
else
  " This is console Vim.
  if exists("+lines")
    set lines=50
  endif
  if exists("+columns")
    set columns=120
  endif
endif
